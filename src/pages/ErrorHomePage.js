import React from 'react';
import PrinterNavBar from "../components/PrinterNavBar";
import * as ReactBootStrap from "react-bootstrap";
import ErrorDropDown from "../components/ErrorDropDown";
import DayDropDown from "../components/DayDropDown";
import Nozzlechart from "../components/Nozzlechart";
import TopErrorsChart from "../components/TopErrorsChart";
import PrinterPrintheadErrorsNavBar from "../components/PrinterPrintheadErrorsNavBar";
import TopErrorCodesHighcharts from "../charts/TopErrorCodesHighcharts";

export default function ErrorHomePage() {

    return(
        <div style={{display: 'flex'}}>
            <PrinterNavBar></PrinterNavBar>
            <ul>
                <TopErrorsChart></TopErrorsChart>
            </ul>
        </div>
    )

}