import React, {useState} from 'react';
import PrinterNavBar from "../components/PrinterNavBar";
import * as ReactBootStrap from "react-bootstrap";
import Errors from "../components/Errors";
import PrinterPrintheadErrorsNavBar from "../components/PrinterPrintheadErrorsNavBar";
import ErrorAPI from "../apis/ErrorAPI";
import ErrorList from "../components/ErrorList";
import TopErrorCodesHighcharts from "../charts/TopErrorCodesHighcharts";

export default function ErrorsPage() {

    return(
        <div style={{display: 'flex'}}>
            <PrinterNavBar></PrinterNavBar>
            <Errors></Errors>
        </div>
    )
}