import React, {useEffect, useState} from 'react';
import PrinterNavBar from '../components/PrinterNavBar.js';
import * as ReactBootStrap from "react-bootstrap";
import DayDropDown from "../components/DayDropDown";
import Nozzlechart from "../components/Nozzlechart";
import MetricDropDown from "../components/MetricDropDown";
import ErrorAPI from "../apis/ErrorAPI";
import UsageAPI from "../apis/UsageAPI";
import FakeHighCharts from "../charts/FakeHighCharts";
import UsageInfoChart from "../charts/UsageInfoChart";

export default function UsageInfoPage() {

    const [inputs, setInputs] = useState({});

    const [daysInUse, setDaysInUse] = useState(0);

    const firstSelectedPrinter = localStorage.getItem("firstSelectedPrinter");

    const secondSelectedPrinter = localStorage.getItem("secondSelectedPrinter");

    const getDaysInUse = inputs => {
            UsageAPI.getDaysInUseByPrinterPrintheadIdUsageDate(inputs)
                .then(response => {
                    setDaysInUse(response.data.usageDIU[0].daysInUse);
                })
                .catch((err) => {
                    console.log(err);
                });
    }

    const handleSubmit = e => {
        e.preventDefault();
        getDaysInUse(inputs)
    }

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }

    return(
        <div style={{ display: 'flex' }}>
            <PrinterNavBar></PrinterNavBar>
            <ul>
                {(secondSelectedPrinter === null)
                    ?<h3 style={{margin:'20px 20px 20px 0px'}}>Printer #{firstSelectedPrinter}</h3>
                    :<h3 style={{margin:'20px 20px 20px 0px'}}>Compare : Printer #{firstSelectedPrinter} and Printer #{secondSelectedPrinter} </h3>
                }
                <ReactBootStrap.Card style={{margin:'20px 20px 20px 0px'}}>
                    <ReactBootStrap.Card.Body>
                        <div style={{display:'inline-flex', alignItems:'center'}}>
                            <ReactBootStrap.Form onSubmit={handleSubmit} style={{display: 'inline-flex', alignItems: 'flex-end', justifyContent: 'center', width:'100%'}}>
                            <ReactBootStrap.Form.Group className="mb-3" controlId="printheadId" style={{margin:'1rem 5px 0rem 5px', width:'350px'}}>
                                <ReactBootStrap.Form.Control name="printheadId" placeholder="Printhead ID" type="number" value={inputs.printheadId || ""} onChange={handleChange} />
                            </ReactBootStrap.Form.Group>

                            <ReactBootStrap.Form.Group className="mb-3" controlId="startDate" style={{margin:'1rem 5px 0rem 5px'}}>
                                <ReactBootStrap.Form.Control name="startDate" type="date" placeholder="Date" value={inputs.startDate || ""} onChange={handleChange} />
                            </ReactBootStrap.Form.Group>

                            <ReactBootStrap.Form.Group className="mb-3" controlId="startDate" style={{margin:'1rem 5px 0rem 5px'}}>
                                <ReactBootStrap.Form.Control name="endDate" type="date" placeholder="Date" value={inputs.endDate || ""} onChange={handleChange} />
                            </ReactBootStrap.Form.Group>

                            <ReactBootStrap.Button variant="danger" type="submit" style={{margin:'10px 100px 15px 10px'}}>
                                Search
                            </ReactBootStrap.Button>
                            </ReactBootStrap.Form>

                            <div style={{display: 'inline-flex', alignItems: 'center', justifyContent: 'center', width:'100%'}}>
                                <h4>Days in use : {daysInUse}</h4>
                            </div>
                        </div>
                    </ReactBootStrap.Card.Body>
                </ReactBootStrap.Card>

                <ReactBootStrap.Card style={{margin:'20px 20px 20px 0px'}}>
                    <ReactBootStrap.Card.Body>
                        <UsageInfoChart></UsageInfoChart>
                    </ReactBootStrap.Card.Body>
                </ReactBootStrap.Card>
                <ReactBootStrap.Card style={{margin:'20px 20px 20px 0px'}}>
                    <ReactBootStrap.Card.Body>
                        <UsageInfoChart></UsageInfoChart>
                    </ReactBootStrap.Card.Body>
                </ReactBootStrap.Card>
                <ReactBootStrap.Card style={{margin:'20px 20px 20px 0px'}}>
                    <ReactBootStrap.Card.Body>
                        <UsageInfoChart></UsageInfoChart>
                    </ReactBootStrap.Card.Body>
                </ReactBootStrap.Card>
            </ul>
        </div>
    )

}
