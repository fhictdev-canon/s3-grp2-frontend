import React from 'react';
import PrinterNavBar from '../components/PrinterNavBar.js';
import Nozzlechart from '../components/Nozzlechart.js';
import * as ReactBootStrap from "react-bootstrap";
import NozzleErrors from "../components/NozzleErrors";
import NozzleErrortype from '../components/NozzleErrortype.js';
import NozzleLogChart from "../charts/NozzleLogChart";

export default function NozzleLogPage() {

    const firstSelectedPrinter = localStorage.getItem("firstSelectedPrinter");

    const secondSelectedPrinter = localStorage.getItem("secondSelectedPrinter");

    return (
        <div style={{ display: 'flex' }}>
            <PrinterNavBar></PrinterNavBar>
            <ul>
                {(secondSelectedPrinter === null)
                    ?<h3 style={{margin:'20px 20px 20px 0px'}}>Printer #{firstSelectedPrinter}</h3>
                    :<h3 style={{margin:'20px 20px 20px 0px'}}>Compare : Printer #{firstSelectedPrinter} and Printer #{secondSelectedPrinter} </h3>
                }
                <ReactBootStrap.Card style={{margin:'20px 20px 20px 0px'}}>
                    <ReactBootStrap.Card.Body>
                        <NozzleLogChart></NozzleLogChart>
                    </ReactBootStrap.Card.Body>
                </ReactBootStrap.Card>
                <NozzleErrors></NozzleErrors>
            </ul>
        </div>
    )

}
