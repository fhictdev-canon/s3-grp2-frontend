import React from "react";
import PrinterNavBar from "../components/PrinterNavBar";
import FakeHighCharts from "../charts/FakeHighCharts";

export default function TestChartsPage() {

    return ( 
        <div style={{display: 'flex'}}>
            <PrinterNavBar/>
            <FakeHighCharts></FakeHighCharts>
        </div>
     )
}