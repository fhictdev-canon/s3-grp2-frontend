import React, { useState } from 'react'
import HighchartsReact from 'highcharts-react-official'
import HighCharts from "highcharts"
import * as ReactBootStrap from "react-bootstrap";
import NozzleErrorAPI from "../apis/NozzleErrorAPI";
import useToasts from '../components/useToasts';

function NozzleLogCharts(props) {

    const { toasts } = useToasts()

    const [inputs, setInputs] = useState({});

    const [chartData, setChartData] = useState();

    const firstSelectedPrinter = localStorage.getItem("firstSelectedPrinter");

    const secondSelectedPrinter = localStorage.getItem("secondSelectedPrinter");

    const getChartData = inputs => {

        inputs.startDate = inputs.startDate + "T00:00:00Z"
        inputs.endDate = inputs.endDate + "T00:00:00Z"

        NozzleErrorAPI.getChartDataByErrorPrinterAndStartEndDate(inputs)
            .then(response => {
                setChartData(response.data.nozzleLogXYData);
                toasts.toastResults()
            })
            .catch((err) => {
                alert("Error : " + err)
                console.log(err);
            });

    }

    const handleSubmit = e => {
        e.preventDefault();
        getChartData(inputs)
    }

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }

    const data2 = [
        { x: '12/23/2020 00:00:00', y: 800 },
        { x: '01/24/2021 01:01:01', y: 500 },
        { x: '02/25/2021 02:02:02', y: 400 },
        { x: '03/26/2021 03:03:03', y: 900 },
        { x: '04/27/2021 04:04:04', y: 1000 },
        { x: '05/28/2021 05:05:05', y: 700 },
        { x: '06/29/2021 06:06:06', y: 600 },
        { x: '07/30/2021 07:07:07', y: 300 },
        { x: '08/31/2021 08:08:08', y: 200 },
        { x: '10/02/2021 09:09:09', y: 100 },
        { x: '11/03/2021 10:10:10', y: 400 },
        { x: '12/04/2021 11:11:11', y: 900 },
        { x: '01/05/2022 12:12:12', y: 1200 },
        { x: '02/06/2022 13:13:13', y: 1400 }
    ]

    data2.forEach(function (el, i) {
        data2[i].x = new Date(el.x).getTime();
    });

    const nozzle = {
        chart: {
            height: 400,
            width: 1000,
            type: 'scatter'
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Nozzle Log'
        },
        xAxis: {
            title: {
                text: 'Time (hrs)'
            },
            type: 'datetime'
        },
        yAxis: {
            title: {
                text: 'Nozzle Number'
            },
            pointStart: 0
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            scatter: {
                jitter: {
                    x: 0.24,
                    y: 0
                },
                marker: {
                    symbol: 'circle'
                },
                tooltip: {
                    pointFormat: 'NozzleLog ID: {point.y}'
                }
            }
        },
        series: [
            {
                color: "#b50000",
                name: 'Printer 1',
                data: chartData,
            },
            /*{
                color: "#0028c9",
                name: 'Printer 2',
                data: data2,
            }*/
        ]
    }

    return (
        <div style={{ display: "inline", width: "500px" }}>
            <ReactBootStrap.Form onSubmit={handleSubmit} style={{ margin: '10px', display: 'flex' }}>
                <ReactBootStrap.Card.Body>
                    <ReactBootStrap.Row>
                        <ReactBootStrap.Col>
                            <ReactBootStrap.Form.Group className="mb-3" controlId="error">
                                <ReactBootStrap.Form.Control
                                    as="select"
                                    name="error"
                                    value={inputs.error || ""}
                                    onChange={handleChange}
                                >
                                    <option>Select Error Type</option>
                                    <option value='"DN"'>DN</option>
                                    <option value='"AN"'>AN</option>
                                    <option value='"BN"'>BN</option>
                                    <option value='"OC"'>OC</option>
                                </ReactBootStrap.Form.Control>
                            </ReactBootStrap.Form.Group>
                        </ReactBootStrap.Col>

                        <ReactBootStrap.Col>
                            <ReactBootStrap.Form.Group className="mb-3" controlId="startDate">
                                <ReactBootStrap.Form.Control name="startDate" type="date" value={inputs.startDate || ""} onChange={handleChange} />
                            </ReactBootStrap.Form.Group>
                        </ReactBootStrap.Col>

                        <ReactBootStrap.Col>
                            <ReactBootStrap.Form.Group className="mb-3" controlId="endDate">
                                <ReactBootStrap.Form.Control name="endDate" type="date" value={inputs.endDate || ""} onChange={handleChange} />
                            </ReactBootStrap.Form.Group>
                        </ReactBootStrap.Col>

                        <ReactBootStrap.Col>
                            <ReactBootStrap.Button variant="danger" type="submit">
                                Filter
                            </ReactBootStrap.Button>
                        </ReactBootStrap.Col>
                    </ReactBootStrap.Row>
                </ReactBootStrap.Card.Body>
            </ReactBootStrap.Form>
            <HighchartsReact highcharts={HighCharts} options={nozzle} />
        </div>
    )
}

export default NozzleLogCharts
