import React, { useState, useEffect } from 'react'
import HighchartsReact from 'highcharts-react-official'
import HighCharts from "highcharts"

function TopErrorCodesHighcharts() {

    const [options, setOptions] = useState({
        chart: {
            height: 800,
            width: 900,
            title: 'Top errors',
            type: 'pie'
        },
        title: {
            text: 'Top error codes'
        },
        series: [
            {
                name: "Amount",
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        var cat = this.series.chart.xAxis[0].categories,
                            x = this.point.x

                        return '#' + cat[x]
                    }
                }
            }
        ],
        plotOptions: {
            pie: {
                allowPointSelect: true,
                showInLegend: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                }
            }
        },
        legend: {
            enabled: true,
            layout: 'vertical',
            align: 'right',

            x: -10,
            y: -250,
            labelFormatter: function () {
                var legendName = this.series.chart.axes[0].categories[this.index];

                return '#' + legendName + ' - Amount: ';
            }
        }
    });

    useEffect(() => {
        function loadData(event) {
            event && event.preventDefault();

            fetch(`http://localhost:8080/printHeadError/getTopErrors/1970-01-01/2022-11-23`)
                .then((res) => res.json())
                .then((res) => parseData(res));
        }
        loadData();
    }, []);



    function parseData(rawData) {
        let categories = [];
        let data = [];

        rawData.errors.forEach((errors) => {
            categories.push(errors.codeError);
            data.push(errors.codeCodesUsage);
        });

        setOptions({
            xAxis: {
                categories: categories
            },
            series: [
                {
                    data: data,
                }
            ]
        })
    }

    return (
        <>
            <HighchartsReact highcharts={HighCharts} options={options} />
        </>
    );

}

export default TopErrorCodesHighcharts;