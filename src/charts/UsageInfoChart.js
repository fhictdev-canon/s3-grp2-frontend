import React, {useState} from 'react'
import HighchartsReact from 'highcharts-react-official'
import HighCharts from "highcharts"
import * as ReactBootStrap from "react-bootstrap";
import UsageAPI from "../apis/UsageAPI";

function UsageInfoChart(props) {

    const [inputs, setInputs] = useState({});

    const [chartDataXFirstPrinter, setChartDataXFirstPrinter] = useState({});

    const [chartDataXSecondPrinter, setChartDataXSecondPrinter] = useState({});

    const firstSelectedPrinter = localStorage.getItem("firstSelectedPrinter");

    const secondSelectedPrinter = localStorage.getItem("secondSelectedPrinter");

    const getChartData = inputs => {

        inputs.printerID = firstSelectedPrinter

        UsageAPI.getChartDataByMetricAndStartEndDate(inputs)
            .then(response => {
                setChartDataXFirstPrinter(response.data.x);
            })
            .catch((err) => {
                alert("Error : " + err)
                console.log(err);
            });

        if (secondSelectedPrinter !== null){
            inputs.printerID = secondSelectedPrinter;
            UsageAPI.getChartDataByMetricAndStartEndDate(inputs)
                .then(response => {
                    setChartDataXSecondPrinter(response.data.x);
                })
                .catch((err) => {
                    alert("Error : " + err)
                    console.log(err);
                });
        }
    }

    const handleSubmit = e => {
        e.preventDefault();
        getChartData(inputs)
    }

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }

    const optionsTest = {
        chart: {
            height: 400,
            width: 1000,
            zoomType: 'y'
        },
        credits : {
            enabled: false
        },
        title: {
            text: '' + inputs.metric
        },
        series: [
            {
                color: "#b50000",
                name: '' + inputs.metric + " - Printer #" + firstSelectedPrinter,
                data: chartDataXFirstPrinter
            },
            {
                color: "#0028c9",
                name: '' + inputs.metric + " - Printer #" + secondSelectedPrinter,
                data: chartDataXSecondPrinter
            }
        ]
    }

    return (
        <div style={{ display: "inline" }}>

            <ReactBootStrap.Form onSubmit={handleSubmit} style={{ margin: '10px', display: 'flex' }}>
                <ReactBootStrap.Card.Body>
                    <ReactBootStrap.Row>
                        <ReactBootStrap.Col>
                            <ReactBootStrap.Form.Group className="mb-3" controlId="printheadId">
                                <ReactBootStrap.Form.Control name="printheadId" placeholder="Printhead ID" type="number" value={inputs.printheadId || ""} onChange={handleChange} />
                            </ReactBootStrap.Form.Group>
                        </ReactBootStrap.Col>
                        <ReactBootStrap.Col>
                            <ReactBootStrap.Form.Group className="mb-3" controlId="metric">
                                <ReactBootStrap.Form.Control
                                    as="select"
                                    name="metric"
                                    value={inputs.metric || ""}
                                    onChange={handleChange}
                                >
                                    <option value="NrOfVibrations">Nr of Vibration Actions</option>
                                    <option value="NrOfJetActuations">Nr of Jet Actuations</option>
                                    <option value="NrOfColdStartups">Nr of Cold Startups</option>
                                    <option value="InkUsage">Ink Usage</option>
                                    <option value="WarmTime">Warm Time</option>
                                    <option value="BiasVoltageTime">Bias Voltage Time</option>
                                </ReactBootStrap.Form.Control>
                            </ReactBootStrap.Form.Group>
                        </ReactBootStrap.Col>

                        <ReactBootStrap.Col>
                            <ReactBootStrap.Form.Group className="mb-3" controlId="startDate">
                                <ReactBootStrap.Form.Control name="startDate" type="date" value={inputs.startDate || ""} onChange={handleChange} />
                            </ReactBootStrap.Form.Group>
                        </ReactBootStrap.Col>

                        <ReactBootStrap.Col>
                            <ReactBootStrap.Form.Group className="mb-3" controlId="endDate">
                                <ReactBootStrap.Form.Control name="endDate" type="date" value={inputs.endDate || ""} onChange={handleChange} />
                            </ReactBootStrap.Form.Group>
                        </ReactBootStrap.Col>

                        <ReactBootStrap.Col>
                            <ReactBootStrap.Button variant="danger" type="submit">
                                Filter
                            </ReactBootStrap.Button>
                        </ReactBootStrap.Col>
                    </ReactBootStrap.Row>
                </ReactBootStrap.Card.Body>
            </ReactBootStrap.Form>

            <HighchartsReact highcharts={HighCharts} options={optionsTest} />
        </div>
    )
}

export default UsageInfoChart