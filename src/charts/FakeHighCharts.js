import React from 'react'
import HighchartsReact from 'highcharts-react-official'
import HighCharts from "highcharts"

function FakeHighCharts(props) {

    const optionsTest = {
        chart: {
            height: 800,
            width: 800,
            zoomType: 'y'
        },
        credits : {
            enabled: false
        },
        title: {
            text: 'Line Chart'
        },
        series: [
            {
                name: 'Test Data',
                data: [0, 100, 200, 300, 200],
            },
            {
                name: 'RT Data',
                data: [0, 250, 320, 160, 240],
            },
        ]
    }

    const options = {
        chart: {
            height: 800,
            width: 800,
            type: 'pie'
        },
        credits : {
            enabled: false
        },
        title: {
            text: 'Pie Chart'
        },
        series: [
            {
                colorByPoint: true,
                name: 'Test Data',
                data: [12, 23, 40, 41]
            },
        ],
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                showInLegend: true,
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        }
    }

    return (
        <div style={{ display: "flex", justifyContent: "center" }}>
            <HighchartsReact highcharts={HighCharts} options={optionsTest} />,
            <HighchartsReact highcharts={HighCharts} options={options} />
        </div>
    )
}

export default FakeHighCharts
