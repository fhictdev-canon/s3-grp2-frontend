import axios from "axios";

const BASE_URL = "http://localhost:8080";

let selectedPrinter = 0;

if (localStorage.getItem("firstSelectedPrinter") === null){
    selectedPrinter = 1;
}else{
    selectedPrinter = localStorage.getItem("firstSelectedPrinter");
}

const ErrorAPI = {
    getErrors: () => axios.get(`${BASE_URL}/printHeadError?printer=${selectedPrinter}`),
    getErrorsByPrinter: printer => axios.get(`${BASE_URL}/printHeadError/printer/${printer}`),
    filterErrorsByDate: dates => axios.get(`${BASE_URL}/printHeadError/${dates.startDate}/${dates.endDate}?printer=${selectedPrinter}`),
    getTotalPrinters: () => axios.get(`${BASE_URL}/printHeadError/nrOfPrinters`),
    verifyUpdatesErrorData : () => axios.get(`${BASE_URL}/printHeadError/status`)
};

export default ErrorAPI;