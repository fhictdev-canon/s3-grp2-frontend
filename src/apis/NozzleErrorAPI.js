import axios from "axios";

const BASE_URL = "http://localhost:8080";

let selectedPrinter = 0;

if (localStorage.getItem("firstSelectedPrinter") === null){
    selectedPrinter = 1;
}else{
    selectedPrinter = localStorage.getItem("firstSelectedPrinter");
}

const NozzleErrorAPI = {
    getNozzleErrors: () => axios.get(`${BASE_URL}/nozzleLog?printer=${selectedPrinter}`),
    filterNozzleErrors: nozzle => axios.get(`${BASE_URL}/nozzleLog/Filters/Range?error=${nozzle.errortype}&nozzle=${nozzle.nozzleOne}&nozzle2=${nozzle.nozzleTwo}&first=${nozzle.startDate}&second=${nozzle.endDate}&printer=${selectedPrinter}`),
    getChartDataByErrorPrinterAndStartEndDate : inputs => axios.get(`${BASE_URL}/nozzleLog/chartData/${inputs.error}/${inputs.startDate}/${inputs.endDate}/${selectedPrinter}`)

};

export default NozzleErrorAPI;