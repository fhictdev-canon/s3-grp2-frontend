import axios from "axios";

const BASE_URL = "http://localhost:8080";

let selectedPrinter = 0;

if (localStorage.getItem("firstSelectedPrinter") === null){
    selectedPrinter = 1;
}else{
    selectedPrinter = localStorage.getItem("firstSelectedPrinter");
}

const UsageAPI = {
    getDaysInUseByPrinterPrintheadIdUsageDate: inputs => axios.get(`${BASE_URL}/printHeadUsage/${inputs.startDate}/${inputs.endDate}/${inputs.printheadId}/${selectedPrinter}`),
    getChartDataByMetricAndStartEndDate: inputs => axios.get(`${BASE_URL}/printHeadUsage/getByParam/${inputs.metric}/${inputs.startDate}/${inputs.endDate}/${inputs.printerID}/${inputs.printheadId}`)
};

export default UsageAPI;