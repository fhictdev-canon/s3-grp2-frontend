import React from "react"
import * as ReactBootStrap from "react-bootstrap";

function NozzleErrorCard(props) {

    return (
        <ReactBootStrap.Card style={{ width: '75rem', margin: '25px 25px 25px 0px'}}>
            <ReactBootStrap.Card.Body>
                <h5 style={{color:'red', margin: '0px 0px 15px 0px'}}>Error : {props.nozzle_error.error}</h5>
                <ReactBootStrap.Card.Text style={{color:'#727272'}}>
                    Printer #{props.nozzle_error.printer}
                </ReactBootStrap.Card.Text>
                <ReactBootStrap.Card.Text style={{color:'#727272'}}>
                    Nozzle number : {props.nozzle_error.nozzle}
                </ReactBootStrap.Card.Text>
                <ReactBootStrap.Card.Text style={{color:'#727272'}}>
                    Timestamp : {props.nozzle_error.timestamp}
                </ReactBootStrap.Card.Text>
            </ReactBootStrap.Card.Body>
        </ReactBootStrap.Card>
    )
}

export default NozzleErrorCard;