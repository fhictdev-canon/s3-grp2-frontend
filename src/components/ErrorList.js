import React from "react"
import ErrorCard from './ErrorCard';

function ErrorList(props) {
    return (
        <ul>
            {props.errors?.map(error => (
                <ErrorCard key={error.id} error={error} />
            ))}
        </ul>
    )
}

export default ErrorList;