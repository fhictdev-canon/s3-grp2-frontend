import React, { useEffect, useState } from "react";
import ErrorList from "./ErrorList";
import ErrorAPI from "../apis/ErrorAPI";
import * as ReactBootStrap from "react-bootstrap";
import { useParams } from "react-router-dom";
import useToasts from "./useToasts";

export default function Errors() {

    const { toasts } = useToasts()

    const [errors, setErrors] = useState([]);

    const [inputs, setInputs] = useState({});

    const [update, setUpdate] = useState({});

    const firstSelectedPrinter = localStorage.getItem("firstSelectedPrinter");

    const secondSelectedPrinter = localStorage.getItem("secondSelectedPrinter");

    useEffect(() => {
        const intervalId = setInterval(() => {
            ErrorAPI.verifyUpdatesErrorData()
                .then(response => {
                    setUpdate(response.data.updated);
                })
                .catch((err) => {
                    console.log(err);
                });
        }, 10000)
    }, []);

    const handleSubmit = e => {
        e.preventDefault();
        filterErrors(inputs);
    }

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }

    const filterErrors = dates => {
        ErrorAPI.filterErrorsByDate(dates)
            .then((response) => {
                setErrors(response.data.errors)
            })
            .catch(err => {
                console.log(err);
            });
    }

    const params = useParams();

    useEffect(() => {
        if (params.id === undefined) {
            ErrorAPI.getErrors()
                .then(response => {
                    setErrors(response.data.errors);
                    toasts.toastUpdate()
                })
                .catch((err) => {
                    console.log(err);
                });
        }
        else {
            ErrorAPI.getErrorsByPrinter(params.id)
                .then(response => {
                    setErrors(response.data.errors);
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    }, [update]);

    return (
        <div style={{ display: 'block', margin: '20px 20px 20px 0px' }}>
            <div style={{ display: 'inline' }}>
                {(secondSelectedPrinter === null)
                    ? <h3 style={{ margin: '0px 20px 20px 30px' }}>Printer #{firstSelectedPrinter}</h3>
                    : <h3 style={{ margin: '0px 20px 20px 30px' }}>Compare : Printer #{firstSelectedPrinter} and Printer #{secondSelectedPrinter} </h3>
                }
                <ReactBootStrap.Form onSubmit={handleSubmit} style={{ margin: '0px 0px 30px 30px' }}>
                    <ReactBootStrap.Card.Body>

                        <ReactBootStrap.Form.Group className="mb-3" controlId="startDate">
                            <ReactBootStrap.Form.Label>From </ReactBootStrap.Form.Label>
                            <ReactBootStrap.Form.Control name="startDate" type="date" value={inputs.startDate || ""} onChange={handleChange} />
                        </ReactBootStrap.Form.Group>

                        <ReactBootStrap.Form.Group className="mb-3" controlId="endDate">
                            <ReactBootStrap.Form.Label> to </ReactBootStrap.Form.Label>
                            <ReactBootStrap.Form.Control name="endDate" type="date" value={inputs.endDate || ""} onChange={handleChange} />
                        </ReactBootStrap.Form.Group>

                        <ReactBootStrap.Button variant="danger" type="submit">
                            Filter
                        </ReactBootStrap.Button>

                    </ReactBootStrap.Card.Body>
                </ReactBootStrap.Form>
            </div>
            {/*<form onSubmit={handleSubmit} class="form-inline">
                        <label htmlFor="startDate" style={{padding: '15px'}}>From</label>
                        <input id="startDate" value={inputs.startDate || ""} className="form-control" type="text" style={{width: '250px'}}/>
                        <label htmlFor="endDate" style={{padding: '15px'}}>To</label>
                        <input id="endDate" value={inputs.endDate || ""} className="form-control" type="text" style={{width: '250px'}}/>
                        <button name="submit" type="submit" className="btn btn-danger" style={{margin: '15px'}}>Filter</button>
                    </form>*/}
            <ErrorList errors={errors} />
        </div>
    )
}