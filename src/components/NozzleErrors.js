import React, { useEffect, useState } from "react";
import NozzleErrorList from "./NozzleErrorList";
import NozzleErrorAPI from "../apis/NozzleErrorAPI";
import * as ReactBootStrap from "react-bootstrap";
import useToasts from "./useToasts";

export default function NozzleErrors() {

    const { toasts } = useToasts()

    const [nozzle_errors, setNozzleErrors] = useState([]);

    const [inputs, setInputs] = useState({});

    const handleSubmit = e => {
        e.preventDefault();
        filterNozzleErrors(inputs);
    }

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }

    const filterNozzleErrors = inputs => {

        inputs.startDate = inputs.startDate + "T00:00:00Z"
        inputs.endDate = inputs.endDate + "T00:00:00Z"

        NozzleErrorAPI.filterNozzleErrors(inputs)
            .then((response) => {
                setNozzleErrors(response.data.nozzleLogs)
                toasts.toastResults()
            })
            .catch(err => {
                console.log(err);
            });
    }

    useEffect(() => {
        NozzleErrorAPI.getNozzleErrors()
            .then(response => {
                setNozzleErrors(response.data.nozzleLogs);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);


    return (
        <div>
            <div style={{ display: "flex", flexWrap: "wrap", justifyContent: "space-between" }}>
                <ReactBootStrap.Form onSubmit={handleSubmit} style={{ margin: '30px' }}>
                    <ReactBootStrap.Card.Body>
                        <ReactBootStrap.Row>
                            <ReactBootStrap.Col>
                                <ReactBootStrap.Form.Group className="mb-3" controlId="nozzleOne">
                                    <ReactBootStrap.Form.Control name="nozzleOne" placeholder="Min nozzle number" type="number" value={inputs.nozzleOne || ""} onChange={handleChange} />
                                </ReactBootStrap.Form.Group>
                            </ReactBootStrap.Col>

                            <ReactBootStrap.Col>
                                <ReactBootStrap.Form.Group className="mb-3" controlId="nozzleTwo">
                                    <ReactBootStrap.Form.Control name="nozzleTwo" placeholder="Max nozzle number" type="number" value={inputs.nozzleTwo || ""} onChange={handleChange} />
                                </ReactBootStrap.Form.Group>
                            </ReactBootStrap.Col>

                            <ReactBootStrap.Col>
                                <ReactBootStrap.Form.Group className="mb-3" controlId="errortype">
                                    <ReactBootStrap.Form.Control
                                        as="select"
                                        name="errortype"
                                        value={inputs.errortype || ""}
                                        onChange={handleChange}
                                    >
                                        <option>Select Error Type</option>
                                        <option value='"DN"'>DN</option>
                                        <option value='"AN"'>AN</option>
                                        <option value='"BN"'>BN</option>
                                        <option value='"OC"'>OC</option>
                                    </ReactBootStrap.Form.Control>
                                </ReactBootStrap.Form.Group>
                            </ReactBootStrap.Col>

                            <ReactBootStrap.Col>
                                <ReactBootStrap.Form.Group className="mb-3" controlId="startDate">
                                    <ReactBootStrap.Form.Control name="startDate" type="date" value={inputs.startDate || ""} onChange={handleChange} />
                                </ReactBootStrap.Form.Group>
                            </ReactBootStrap.Col>

                            <ReactBootStrap.Col>
                                <ReactBootStrap.Form.Group className="mb-3" controlId="endDate">
                                    <ReactBootStrap.Form.Control name="endDate" type="date" value={inputs.endDate || ""} onChange={handleChange} />
                                </ReactBootStrap.Form.Group>
                            </ReactBootStrap.Col>

                            <ReactBootStrap.Col>
                                <ReactBootStrap.Button variant="danger" type="submit">
                                    Filter
                                </ReactBootStrap.Button>
                            </ReactBootStrap.Col>

                        </ReactBootStrap.Row>

                    </ReactBootStrap.Card.Body>
                </ReactBootStrap.Form>
                <NozzleErrorList nozzle_errors={nozzle_errors} />
            </div>

        </div>
    )
}