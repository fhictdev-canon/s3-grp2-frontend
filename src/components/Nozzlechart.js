import React from "react"
import { XYPlot, MarkSeries, VerticalGridLines, HorizontalGridLines, YAxis, XAxis} from 'react-vis';
import 'react-vis/dist/style.css';

function Nozzlecharts(){

    const data1 = [     
        {x: 0, y: 8},
        {x: 1, y: 5},
        {x: 2, y: 4},
        {x: 3, y: 9},
        {x: 4, y: 1},
        {x: 5, y: 7},
        {x: 6, y: 6},
        {x: 7, y: 3},
        {x: 8, y: 2},
        {x: 9, y: 1},
        {x: 10, y: 4},
        {x: 11, y: 9},
        {x: 12, y: 12},
        {x: 13, y: 14}
    ]

    return (
        <div style={{padding:'20px'}}>
            <XYPlot height={250} width={1100}>
            <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis />
                <YAxis />
                <MarkSeries data={data1}
                style={{stroke:"black", fill:"red"}}
                />
            </XYPlot>
        </div>
    )
}

export default Nozzlecharts;