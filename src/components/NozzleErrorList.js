import React from "react"
import NozzleErrorCard from './NozzleErrorCard';

function NozzleErrorList(props) {
    return (
        <ul style={{ margin: '0px 0px 0px -30px'}}>
            {props.nozzle_errors?.map(nozzle_error => (
                <NozzleErrorCard key={nozzle_error.id} nozzle_error={nozzle_error} />
            ))}
        </ul>
    )
}

export default NozzleErrorList;