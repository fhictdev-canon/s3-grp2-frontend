import React, { useState } from 'react'
import { toast } from 'react-toastify'

function useToasts() {


    const toastUpdate = () => toast.info('Updated', {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
        progress: undefined,
        theme: "colored",
    })

    const toastResults = () => toast.info('Here are the results of your search', {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
        progress: undefined,
        theme: "colored",
    })

    const toastSecondPrinterDeleted = () => toast.info('Second printer deleted', {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: false,
        progress: undefined,
        theme: "colored",
    })

    const toasts = { toastUpdate, toastResults, toastSecondPrinterDeleted }

    return {
        toasts
    }
}

export default useToasts