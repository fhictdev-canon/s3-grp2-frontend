import React from 'react';
import * as ReactBootStrap from "react-bootstrap";
import styles from './PrinterNavBar.module.css'

function PrinterPrintheadErrorsNavBar() {


    return (
        <html>
        <div style={{ width: '250px', padding: '30px', height: '100vh', background: '#c9c9c9', position: "sticky",top: 0}}>
            <ReactBootStrap.Nav defaultActiveKey="" className="flex-column">
                <ReactBootStrap.Form className="d-flex" style={{ margin: '0px 0px 15px 0px' }}>
                    <ReactBootStrap.Form.Control
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                    />
                    <ReactBootStrap.Button variant="outline-danger">Search</ReactBootStrap.Button>
                </ReactBootStrap.Form>
                <ReactBootStrap.Nav.Link style={{ padding: '12px' }} class="link-secondary" className={styles} href="/errors">All errors</ReactBootStrap.Nav.Link>
                <ReactBootStrap.Nav.Link style={{ padding: '12px' }} class="link-secondary" className={styles} href="/errors/1">Printer #1</ReactBootStrap.Nav.Link>
                <ReactBootStrap.Nav.Link style={{ padding: '12px' }} class="link-secondary" className={styles} href="/errors/2">Printer #2</ReactBootStrap.Nav.Link>
                <ReactBootStrap.Nav.Link style={{ padding: '12px' }} class="link-secondary" className={styles} href="/errors/3">Printer #3</ReactBootStrap.Nav.Link>
                <ReactBootStrap.Nav.Link style={{ padding: '12px' }} class="link-secondary" className={styles} href="/errors/4">Printer #4</ReactBootStrap.Nav.Link>
                <ReactBootStrap.Nav.Link style={{ padding: '12px' }} class="link-secondary" className={styles} href="/errors/5">Printer #5</ReactBootStrap.Nav.Link>
                <ReactBootStrap.Nav.Link style={{ padding: '12px' }} class="link-secondary" className={styles} href="/errors/6">Printer #6</ReactBootStrap.Nav.Link>
                <ReactBootStrap.Nav.Link style={{ padding: '12px' }} class="link-secondary" className={styles} href="/errors/7">Printer #7</ReactBootStrap.Nav.Link>
            </ReactBootStrap.Nav>
        </div>
        </html>
    )

}

export default PrinterPrintheadErrorsNavBar;