import React from "react"
import * as ReactBootStrap from "react-bootstrap";
import { Link } from 'react-router-dom';

function NavBar() {

    return (
        <ReactBootStrap.Navbar bg="light" expand="lg">
            <ReactBootStrap.Container style={{padding: '10px'}}>
                <ReactBootStrap.Navbar.Brand href="">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Canon_Production_Printing_wordmark.svg/2560px-Canon_Production_Printing_wordmark.svg.png" width="200" height="30" alt=""></img>
                </ReactBootStrap.Navbar.Brand>
                <ReactBootStrap.Navbar.Toggle aria-controls="basic-navbar-nav" />
                <ReactBootStrap.Navbar.Collapse id="basic-navbar-nav">
                    <ReactBootStrap.Nav className="mr-auto">
                        <ReactBootStrap.Nav.Link as={Link} to="usageinfo">Printhead Usage Info</ReactBootStrap.Nav.Link>
                        <ReactBootStrap.Nav.Link as={Link} to="nozzlelog">Nozzle Logging</ReactBootStrap.Nav.Link>
                        <ReactBootStrap.NavDropdown title="Printhead Errors" id="navbarScrollingDropdown">
                            <ReactBootStrap.NavDropdown.Item as={Link} to="toperrors">Top 10 Errors</ReactBootStrap.NavDropdown.Item>
                            <ReactBootStrap.NavDropdown.Item as={Link} to="errors">
                                List of errors
                            </ReactBootStrap.NavDropdown.Item>
                        </ReactBootStrap.NavDropdown>
                    </ReactBootStrap.Nav>
                </ReactBootStrap.Navbar.Collapse>
            </ReactBootStrap.Container>
        </ReactBootStrap.Navbar>
    )
}

export default NavBar;