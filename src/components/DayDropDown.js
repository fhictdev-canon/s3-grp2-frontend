import Dropdown from 'react-bootstrap/Dropdown';
import React from 'react'

function DayDropDown() {
  return (
    <Dropdown style={{ margin:'5px'}}>
      <Dropdown.Toggle variant="outline-secondary" id="dropdown-basic">
        Time frame
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item href="#/action-1">Last Day</Dropdown.Item>
        <Dropdown.Item href="#/action-2">Last Week</Dropdown.Item>
        <Dropdown.Item href="#/action-3">Last Month</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default DayDropDown;