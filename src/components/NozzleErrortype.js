import React, {useEffect, useState} from "react";
import NozzleErrorList from "./NozzleErrorList";
import NozzleErrorAPI from "../apis/NozzleErrorAPI";
import * as ReactBootStrap from "react-bootstrap";

export default function NozzleErrortype() {

    const [nozzle_errors, setNozzleErrors] = useState([]);

    const [inputs, setInputs] = useState({});

    const handleSubmit = e => {
        e.preventDefault();
        filterNozzleErrors(inputs);
    }

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }

    const filterNozzleErrors = dates => {
        NozzleErrorAPI.filterNozzleErrorsByNozzleError(dates)
            .then((response) => {
                setNozzleErrors(response.data.nozzleLogs)
            })
            .catch(err => {
                console.log(err);
            });
    }

    useEffect(() => {
        NozzleErrorAPI.getNozzleErrors()
            .then(response => {
                setNozzleErrors(response.data.nozzleLogs);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);


    return(
        <div>
            <ReactBootStrap.Form onSubmit={handleSubmit} style={{ margin: '30px' }}>
                <ReactBootStrap.Card.Body>

                    <ReactBootStrap.Form.Group className="mb-3" controlId="errortype">
                        <ReactBootStrap.Form.Label>error type </ReactBootStrap.Form.Label>
                        <ReactBootStrap.Form.Control name="errortype" type="text" value={inputs.errortype || ""} onChange={handleChange} />
                    </ReactBootStrap.Form.Group>

                    <ReactBootStrap.Button variant="danger" type="submit">
                        Filter
                    </ReactBootStrap.Button>

                </ReactBootStrap.Card.Body>
            </ReactBootStrap.Form>
        <NozzleErrorList nozzle_errors={nozzle_errors} />
        </div>
    )
}