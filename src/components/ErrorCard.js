import React from "react"
import * as ReactBootStrap from "react-bootstrap";

function ErrorCard(props) {

    return (
        <ReactBootStrap.Card style={{ width: '75rem', margin: '25px 25px 25px 0px'}}>
            <ReactBootStrap.Card.Body>
                <h5 style={{color:'red', margin: '0px 0px 15px 0px'}}>Error #{props.error.errorCode} <span className="badge bg-light text-dark">Printhead ID : {props.error.printheadId}</span></h5>
                <ReactBootStrap.Card.Text style={{color:'#727272'}}>
                    Printer #{props.error.printer}
                </ReactBootStrap.Card.Text>
                <ReactBootStrap.Card.Text style={{color:'#727272'}}>
                    Timestamp of usage information : {props.error.timestamp}
                </ReactBootStrap.Card.Text>
                <ReactBootStrap.Card.Text style={{color:'#727272'}}>
                    Error event number : {props.error.errorEventNr}
                </ReactBootStrap.Card.Text>
            </ReactBootStrap.Card.Body>
        </ReactBootStrap.Card>
    )
}

export default ErrorCard;