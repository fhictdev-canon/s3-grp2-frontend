import React, { useEffect, useState } from "react"
import * as ReactBootStrap from "react-bootstrap";
import HighchartsReact from "highcharts-react-official";
import HighCharts from "highcharts"
import ErrorAPI from "../apis/ErrorAPI";

function TopErrorsChart() {

    const [isLoading, setLoading] = useState(true);
    const [inputs, setInputs] = useState({});
    const [update, setUpdate] = useState({});

    useEffect(() => {
        const intervalId = setInterval(() => {
        ErrorAPI.verifyUpdatesErrorData()
            .then(response => {
                setUpdate(response.data.updated);
            })
            .catch((err) => {
                console.log(err);
            });}, 10000)
    }, []);

    const [options, setOptions] = useState({
        chart: {
            height: 800,
            width: 900,
            title: 'Top errors',
            type: 'pie'
        },
        title: {
            text: ''
        },
        series: [
            {
                name: "Amount",
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        var cat = this.series.chart.xAxis[0].categories,
                            x = this.point.x

                        return '#' + cat[x]
                    }
                }
            }
        ],
        plotOptions: {
            pie: {
                allowPointSelect: true,
                showInLegend: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                }
            }
        },
        legend: {
            title: {
                text: 'Errors<br/><span style="font-size: 16px; color: #666; font-weight: normal">(Click to hide)</span>'
            },
            enabled: true,
            layout: 'vertical',
            align: 'right',
            x: -10,
            y: -250,
            labelFormatter: function () {
                var legendName = this.series.chart.axes[0].categories[this.index];

                return '#' + legendName + ' - Amount: ' + this.y;
            }
        }
    });

    useEffect(() => {
        setLoading(true)
        try {
            fetch(`http://localhost:8080/printHeadError/getTopErrors/1970-01-01/2022-11-23`)
                .then((res) => res.json())
                .then((res) => parseData(res))
                setLoading(false)
        } catch (e) {
            console.log(e)
            setLoading(true)
        }
    }, [update]);

    function filterErrors(event) {
        setLoading(true)
        event && event.preventDefault();

        fetch(`http://localhost:8080/printHeadError/getTopErrors/${inputs.startDate}/${inputs.endDate}`)
            .then((res) => res.json())
            .then((res) => parseData(res))
            setLoading(false)
    }

    function parseData(rawData) {

        let categories = [];
        let data = [];

        rawData.errors.forEach((errors) => {
            categories.push(errors.codeError);
            data.push(errors.codeCodesUsage);
        });

        setOptions({
            xAxis: {
                categories: categories
            },
            series: [
                {
                    data: data
                }
            ]
        })
    }

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({ ...values, [name]: value }))
    }

    const handleSubmit = e => {
        e.preventDefault();
        filterErrors()
    }

    return (
        isLoading ? <p>Loading...</p> :
            <div style={{ display: 'block' }}>
                <ReactBootStrap.Form onSubmit={handleSubmit} style={{ margin: '30px' }}>
                    <ReactBootStrap.Card.Body>
                        <ReactBootStrap.Form.Group className="mb-3" controlId="startDate">
                            <ReactBootStrap.Form.Label>From </ReactBootStrap.Form.Label>
                            <ReactBootStrap.Form.Control name="startDate" type="date" value={inputs.startDate || ""} onChange={handleChange} />
                        </ReactBootStrap.Form.Group>
                        <ReactBootStrap.Form.Group className="mb-3" controlId="endDate">
                            <ReactBootStrap.Form.Label> to </ReactBootStrap.Form.Label>
                            <ReactBootStrap.Form.Control name="endDate" type="date" value={inputs.endDate || ""} onChange={handleChange} />
                        </ReactBootStrap.Form.Group>
                        <ReactBootStrap.Button variant="danger" type="submit">
                            Filter
                        </ReactBootStrap.Button>
                    </ReactBootStrap.Card.Body>
                </ReactBootStrap.Form>
                <ReactBootStrap.Card style={{ margin: '20px 20px 20px 0px' }}>
                    <ReactBootStrap.Card.Body>
                        <h4 style={{ margin: '10px' }}>Top error codes</h4>
                    </ReactBootStrap.Card.Body>
                    <HighchartsReact highcharts={HighCharts} options={options} />
                </ReactBootStrap.Card>
            </div>
    )
}


export default TopErrorsChart;