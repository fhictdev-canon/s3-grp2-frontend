import React from 'react'
import Dropdown from 'react-bootstrap/Dropdown';

function ErrorDropDown() {
  return (
    <Dropdown style={{ margin:'5px'}}>
      <Dropdown.Toggle variant="outline-secondary" id="dropdown-basic">
        Type of error
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item href="#/action-1">DN</Dropdown.Item>
        <Dropdown.Item href="#/action-2">AN</Dropdown.Item>
        <Dropdown.Item href="#/action-3">BN</Dropdown.Item>
        <Dropdown.Item href="#/action-4">OC</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default ErrorDropDown;