import React, {useEffect, useState} from 'react';
import * as ReactBootStrap from "react-bootstrap";
import styles from './PrinterNavBar.module.css'
import ErrorAPI from "../apis/ErrorAPI";

function PrinterNavBar() {

    const currentLocation = document.location.href;
    const [totalPrinters, setTotalPrinters] = useState([]);
    const [selectedPrinter, setSelectedPrinter] = useState([]);
    let printer = 0;


    useEffect(() => {
        ErrorAPI.getTotalPrinters()
            .then(response => {
                setTotalPrinters(response.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    function searchPrinter() {
        var input, filter, ulOne, liOne, a, i, txtValue, ulTwo, liTwo;
        input = document.getElementById('printerInput');
        filter = input.value.toUpperCase();
        ulOne = document.getElementById("printerULFirst");
        liOne = ulOne.getElementsByTagName('li');
        ulTwo = document.getElementById("printerULSecond");
        liTwo = ulTwo.getElementsByTagName('li');


        for (i = 0; i < liOne.length; i++) {
            a = liOne[i].getElementsByTagName("a")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                liOne[i].style.display = "";
            } else {
                liOne[i].style.display = "none";
            }
        }

        for (i = 0; i < liTwo.length; i++) {
            a = liTwo[i].getElementsByTagName("a")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                liTwo[i].style.display = "";
            } else {
                liTwo[i].style.display = "none";
            }
        }
    }

    const selectFirstPrinter = printer => {
        localStorage.setItem("firstSelectedPrinter",printer);
        window.location = currentLocation;
    }

    const selectSecondPrinter = printer => {
        localStorage.setItem("secondSelectedPrinter",printer);
        window.location = currentLocation;
    }

    const deleteSecondPrinter = () => {
        alert("Second printer deleted")
        localStorage.removeItem("secondSelectedPrinter");
        window.location = currentLocation;
    }

    const displayPrinterList = numberOfPrinters => {
        let i;
        let content = [];
        for(i = 1; i <= numberOfPrinters; i++) {
            content.push(<li id={"printer" + i} style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter(document.getElementById("printer" + i).textContent)} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #{i}</a></li>);
        }
        return content;
    };

    return (
        <html>
            <div style={{ width: '275px', padding: '30px', height: '100vh', background: '#c9c9c9', position: "sticky",top: 0}}>
                <input style={{ margin:'5px 0px 20px 0px', borderRadius: '7px', height:'40px'}}type="text" id="printerInput" onKeyUp={searchPrinter} placeholder="Search for printer.."></input>
                <div style={{ display:'flex', direction:'row', justifyContent:'flex-start', alignItems:'flex-start' }}>
                    <ul id="printerULFirst" style={{ margin:'5px 0px 30px -20px', listStyleType: "none"}}>
                        {/*{displayPrinterList(totalPrinters)}*/}
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter('0')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #0</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter('1')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #1</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter('2')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #2</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter('3')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #3</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter('4')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #4</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter('5')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #5</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter('6')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #6</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter('7')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #7</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter('8')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #8</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectFirstPrinter('9')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #9</a></li>
                    </ul>
                    <ul id="printerULSecond" style={{ margin:'5px 0px 30px -20px', listStyleType: "none"}}>
                        {/*{displayPrinterList(totalPrinters)}*/}
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectSecondPrinter('0')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #0</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectSecondPrinter('1')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #1</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectSecondPrinter('2')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #2</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectSecondPrinter('3')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #3</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectSecondPrinter('4')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #4</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectSecondPrinter('5')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #5</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectSecondPrinter('6')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #6</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectSecondPrinter('7')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #7</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectSecondPrinter('8')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #8</a></li>
                        <li style={{ margin:'25px 0px 25px 0px'}}><a href="" onClick={() => selectSecondPrinter('9')} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Printer #9</a></li>
                    </ul>
                </div>
                <a href="" onClick={deleteSecondPrinter} style={{color:'grey', fontSize:'17px', textDecoration:'none'}}>Remove second printer</a>
            </div>
        </html>
    )

}

export default PrinterNavBar;