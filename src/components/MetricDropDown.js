import Dropdown from 'react-bootstrap/Dropdown';
import React from 'react'

function DayDropDown() {
    return (
        <Dropdown style={{ margin:'5px'}}>
            <Dropdown.Toggle variant="outline-secondary" id="dropdown-basic">
                Metric
            </Dropdown.Toggle>

            <Dropdown.Menu>
                <Dropdown.Item href="#/action-1">Nr of Vibration Actions</Dropdown.Item>
                <Dropdown.Item href="#/action-2">Nr of Jet Actuations</Dropdown.Item>
                <Dropdown.Item href="#/action-3">Nr of Cold Startups</Dropdown.Item>
                <Dropdown.Item href="#/action-1">Ink Usage</Dropdown.Item>
                <Dropdown.Item href="#/action-2">Warm Time</Dropdown.Item>
                <Dropdown.Item href="#/action-3">Bias Voltage Time</Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
    );
}

export default DayDropDown;