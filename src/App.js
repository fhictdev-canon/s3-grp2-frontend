import React from 'react'
import './App.css';
import { Route, Routes, BrowserRouter as Router } from "react-router-dom";
import NavBar from './components/NavBar';
import ErrorsPage from './pages/ErrorsPage';
import UsageInfoPage from './pages/UsageInfoPage.js';
import NozzleLogPage from './pages/NozzleLogPage.js';
import TestChartsPage from './pages/TestChartsPage';
import ErrorHomePage from "./pages/ErrorHomePage";
import { ToastContainer } from 'react-toastify';


function App() {
    return (
        <Router>
            <NavBar />
            <Routes>
                <Route path="/usageinfo" element={<UsageInfoPage />} />
                <Route path="/nozzlelog" element={<NozzleLogPage />} />
                <Route exact path="/errors" element={<ErrorsPage />} />
                <Route path="/errors/:id" element={<ErrorsPage />} />
                <Route path="/testcharts" element={<TestChartsPage />} />
                <Route path="/toperrors" element={<ErrorHomePage />} />
                <Route exact path="/" element={<UsageInfoPage />} />
            </Routes>
            <ToastContainer />
        </Router>
    );
}

export default App;
